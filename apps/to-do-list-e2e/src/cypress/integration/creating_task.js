describe('Creating task and changing its status', () => {
  it('creates a task', () => {
    cy.visit('/')

    cy.get('[data-cy="new-task-button"]')
      .click()

    cy.get('[data-cy="task-dialog"]')
      .get('[data-cy="task-name-input"]')
      .type('Test Task')

    cy.get('[data-cy="task-select"]')
      .click()
      .get('[data-cy="task-option"]')
      .contains('Low')
      .click()

    cy.get('[data-cy="add-button"]')
      .click()

    cy.get('[data-cy="to-do-list"]')
      .should('contain', 'Test Task')
  })

  it('changes task name and priority', () => {
    cy.get('[data-cy="to-do-list"]')
      .get('[data-cy="mat-list-item"]')
      .get('[data-cy="edit-button"]')
      .click()

    cy.get('[data-cy="task-dialog"]')
      .get('[data-cy="task-name-input"]')
      .clear()
      .type('Important Task')

    cy.get('[data-cy="task-select"]')
      .click()
      .get('[data-cy="task-option"]')
      .contains('High')
      .click()

    cy.get('[data-cy="add-button"]')
      .click()

    cy.get('[data-cy="to-do-list"]')
      .should('contain', 'Important Task')
  })

  it('changes task status', () => {
    cy.get('[data-cy="to-do-list"]')
      .get('[data-cy="mat-list-item"]')
      .get('[data-cy="change-status-button"]')
      .click()

    cy.get('[data-cy="done-list"]')
      .should('contain', 'Important Task')
  })

  it('deletes task', () => {
    cy.get('[data-cy="done-list"]')
      .get('[data-cy="mat-list-item"]')
      .get('[data-cy="delete-button"]')
      .click()

    cy.get('[data-cy="confirm-button"]')
      .click()

    cy.get('[data-cy="to-do-list"]')
      .should('not.contain', 'Important Task')
  })
})
