import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../environments/environment';
import { ToDoListWebShellModule } from '@valueadd/to-do-list/web/shell';
import { ToDoListWebDataAccessModule } from '@valueadd/to-do-list/web/data-access';
import { CoreModule } from '../../../../libs/to-do-list/web/shared/core/src/lib/core.module';
import { ToDoListWebShellRoutingModule } from '../../../../libs/to-do-list/web/shell/src/lib/to-do-list-web-shell/to-do-list-web-shell-routing.module';


@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ToDoListWebShellModule,
    ToDoListWebShellRoutingModule,
    StoreDevtoolsModule.instrument({ maxAge: 25, logOnly: environment.production }),
    ToDoListWebDataAccessModule,
    CoreModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
