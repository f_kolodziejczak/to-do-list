import { Component } from '@angular/core';

@Component({
  selector: 'valueadd-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'feature-list';
}
