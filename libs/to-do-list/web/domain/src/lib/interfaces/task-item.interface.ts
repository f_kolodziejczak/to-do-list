import { Priority } from '../enums/priority.enum.';
import { Status } from '../enums/status.enum';

export interface TaskItem {
  id: string;
  text: string;
  priority: Priority;
  status: Status;
}
