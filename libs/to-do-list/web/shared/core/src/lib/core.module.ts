import { NgModule } from '@angular/core';
import { NxModule } from '@nrwl/angular';
import { StoreModule } from '@ngrx/store';


@NgModule({
  imports: [
    NxModule.forRoot(),
    StoreModule.forRoot(
      {},
      {}
    )
  ],
})
export class CoreModule { }
