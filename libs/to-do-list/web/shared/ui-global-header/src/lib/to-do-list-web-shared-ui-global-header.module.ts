import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GlobalHeaderComponent } from './global-header/global-header.component';
import { MatToolbarModule } from '@angular/material/toolbar';

@NgModule({
  declarations: [GlobalHeaderComponent],
  exports: [GlobalHeaderComponent],
  imports: [CommonModule, MatToolbarModule]
})
export class ToDoListWebSharedUiGlobalHeaderModule {}
