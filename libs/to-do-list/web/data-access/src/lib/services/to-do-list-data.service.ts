import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { LocalStorageClient } from '@valueadd/common';
import { GetTaskItemRequestPayload } from '../resources/request-payloads/get-task-item.request-payload';
import { CreateTaskItemRequestPayload } from '../resources/request-payloads/create-task-item.request-payload';
import { UpdateTaskItemRequestPayload } from '../resources/request-payloads/update-task-item.request-payload';
import { RemoveTaskItemRequestPayload } from '../resources/request-payloads/remove-task-item.request-payload';
import { TaskItem } from '../../../../domain/src';

@Injectable()
export class ToDoListDataService {
  readonly collections = {
    taskItem: 'taskItem',
  };

  getTaskItem(payload: GetTaskItemRequestPayload): Observable<TaskItem> {
    return LocalStorageClient.get(this.collections.taskItem, payload.id);
  }

  getTaskItemCollection(): Observable<TaskItem[]> {
    return LocalStorageClient.getAll(this.collections.taskItem);
  }

  createTaskItem(payload: CreateTaskItemRequestPayload): Observable<TaskItem> {
    return LocalStorageClient.save(this.collections.taskItem, payload.data);
  }

  updateTaskItem(payload: UpdateTaskItemRequestPayload): Observable<TaskItem> {
    return LocalStorageClient.update(this.collections.taskItem, payload.data);
  }

  removeTaskItem(payload: RemoveTaskItemRequestPayload): Observable<null> {
    return LocalStorageClient.remove(this.collections.taskItem, payload.id);
  }
}
