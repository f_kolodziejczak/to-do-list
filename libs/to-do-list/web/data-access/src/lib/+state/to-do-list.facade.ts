import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { ToDoListPartialState } from './to-do-list.reducer';
import { toDoListQuery } from './to-do-list.selectors';
import { fromToDoListActions } from './to-do-list.actions';
import { GetTaskItemRequestPayload } from '../resources/request-payloads/get-task-item.request-payload';
import { CreateTaskItemRequestPayload } from '../resources/request-payloads/create-task-item.request-payload';
import { UpdateTaskItemRequestPayload } from '../resources/request-payloads/update-task-item.request-payload';
import { RemoveTaskItemRequestPayload } from '../resources/request-payloads/remove-task-item.request-payload';

@Injectable()
export class ToDoListFacade {
  taskItem$ = this.store.pipe(select(toDoListQuery.getTaskItem));
  taskItemLoading$ = this.store.pipe(select(toDoListQuery.getTaskItemLoading));
  taskItemLoadError$ = this.store.pipe(
    select(toDoListQuery.getTaskItemLoadError)
  );
  taskItemCollectionToDo$ = this.store.pipe(
    select(toDoListQuery.getTaskItemCollectionToDo)
  );
  taskItemCollectionDone$ = this.store.pipe(
    select(toDoListQuery.getTaskItemCollectionDone)
  );
  taskItemCollection$ = this.store.pipe(
    select(toDoListQuery.getTaskItemCollection)
  );
  taskItemCollectionLoading$ = this.store.pipe(
    select(toDoListQuery.getTaskItemCollectionLoading)
  );
  taskItemCollectionLoadError$ = this.store.pipe(
    select(toDoListQuery.getTaskItemCollectionLoadError)
  );
  taskItemCreating$ = this.store.pipe(
    select(toDoListQuery.getTaskItemCreating)
  );
  taskItemCreateError$ = this.store.pipe(
    select(toDoListQuery.getTaskItemCreateError)
  );
  taskItemUpdating$ = this.store.pipe(
    select(toDoListQuery.getTaskItemUpdating)
  );
  taskItemUpdateError$ = this.store.pipe(
    select(toDoListQuery.getTaskItemUpdateError)
  );
  taskItemRemoving$ = this.store.pipe(
    select(toDoListQuery.getTaskItemRemoving)
  );
  taskItemRemoveError$ = this.store.pipe(
    select(toDoListQuery.getTaskItemRemoveError)
  );
  constructor(private store: Store<ToDoListPartialState>) {}

  getTaskItem(data: GetTaskItemRequestPayload): void {
    this.store.dispatch(new fromToDoListActions.GetTaskItem(data));
  }

  getTaskItemCollection(): void {
    this.store.dispatch(new fromToDoListActions.GetTaskItemCollection());
  }

  createTaskItem(data: CreateTaskItemRequestPayload): void {
    this.store.dispatch(new fromToDoListActions.CreateTaskItem(data));
  }

  updateTaskItem(data: UpdateTaskItemRequestPayload): void {
    this.store.dispatch(new fromToDoListActions.UpdateTaskItem(data));
  }

  removeTaskItem(data: RemoveTaskItemRequestPayload): void {
    this.store.dispatch(new fromToDoListActions.RemoveTaskItem(data));
  }
}
