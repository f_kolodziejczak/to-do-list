import { TestBed } from '@angular/core/testing';
import { Observable } from 'rxjs';
import { provideMockActions } from '@ngrx/effects/testing';
import { provideMockStore } from '@ngrx/store/testing';
import { NxModule, DataPersistence } from '@nrwl/angular';
import { cold, hot } from 'jest-marbles';
import { ToDoListEffects } from './to-do-list.effects';
import { fromToDoListActions } from './to-do-list.actions';
import { ToDoListDataService } from '../services/to-do-list-data.service';
import { createSpyObj } from 'jest-createspyobj';

describe('ToDoListEffects', () => {
  let toDoListDataService: jest.Mocked<ToDoListDataService>;
  let actions: Observable<any>;
  let effects: ToDoListEffects;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [NxModule.forRoot()],
      providers: [
        ToDoListEffects,
        DataPersistence,
        provideMockActions(() => actions),
        provideMockStore({ initialState: {} }),
        {
          provide: ToDoListDataService,
          useValue: createSpyObj(ToDoListDataService),
        },
      ],
    });

    effects = TestBed.inject(ToDoListEffects);
    toDoListDataService = TestBed.inject(
      ToDoListDataService
    ) as jest.Mocked<ToDoListDataService>;
  });

  describe('getTaskItem$', () => {
    test('returns GetTaskItemSuccess action on success', () => {
      const payload = {} as any;
      const action = new fromToDoListActions.GetTaskItem({} as any);
      const completion = new fromToDoListActions.GetTaskItemSuccess(payload);

      actions = hot('-a', { a: action });
      const response = cold('--b|', { b: payload });
      const expected = cold('---c', { c: completion });
      toDoListDataService.getTaskItem.mockReturnValue(response);

      expect(effects.getTaskItem$).toSatisfyOnFlush(() => {
        expect(toDoListDataService.getTaskItem).toHaveBeenCalled();
      });
      expect(effects.getTaskItem$).toBeObservable(expected);
    });

    test('returns GetTaskItemFail action on fail', () => {
      const payload = {} as any;
      const action = new fromToDoListActions.GetTaskItem({} as any);
      const completion = new fromToDoListActions.GetTaskItemFail(payload);

      actions = hot('-a', { a: action });
      const response = cold('-#', {}, payload);
      const expected = cold('--c', { c: completion });
      toDoListDataService.getTaskItem.mockReturnValue(response);

      expect(effects.getTaskItem$).toSatisfyOnFlush(() => {
        expect(toDoListDataService.getTaskItem).toHaveBeenCalled();
      });
      expect(effects.getTaskItem$).toBeObservable(expected);
    });
  });

  describe('getTaskItemCollection$', () => {
    test('returns GetTaskItemCollectionSuccess action on success', () => {
      const payload = {} as any;
      const action = new fromToDoListActions.GetTaskItemCollection({} as any);
      const completion = new fromToDoListActions.GetTaskItemCollectionSuccess(
        payload
      );

      actions = hot('-a', { a: action });
      const response = cold('--b|', { b: payload });
      const expected = cold('---c', { c: completion });
      toDoListDataService.getTaskItemCollection.mockReturnValue(response);

      expect(effects.getTaskItemCollection$).toSatisfyOnFlush(() => {
        expect(toDoListDataService.getTaskItemCollection).toHaveBeenCalled();
      });
      expect(effects.getTaskItemCollection$).toBeObservable(expected);
    });

    test('returns GetTaskItemCollectionFail action on fail', () => {
      const payload = {} as any;
      const action = new fromToDoListActions.GetTaskItemCollection({} as any);
      const completion = new fromToDoListActions.GetTaskItemCollectionFail(
        payload
      );

      actions = hot('-a', { a: action });
      const response = cold('-#', {}, payload);
      const expected = cold('--c', { c: completion });
      toDoListDataService.getTaskItemCollection.mockReturnValue(response);

      expect(effects.getTaskItemCollection$).toSatisfyOnFlush(() => {
        expect(toDoListDataService.getTaskItemCollection).toHaveBeenCalled();
      });
      expect(effects.getTaskItemCollection$).toBeObservable(expected);
    });
  });

  describe('createTaskItem$', () => {
    test('returns CreateTaskItemSuccess action on success', () => {
      const payload = {} as any;
      const action = new fromToDoListActions.CreateTaskItem({} as any);
      const completion = new fromToDoListActions.CreateTaskItemSuccess(payload);

      actions = hot('-a', { a: action });
      const response = cold('--b|', { b: payload });
      const expected = cold('---c', { c: completion });
      toDoListDataService.createTaskItem.mockReturnValue(response);

      expect(effects.createTaskItem$).toSatisfyOnFlush(() => {
        expect(toDoListDataService.createTaskItem).toHaveBeenCalled();
      });
      expect(effects.createTaskItem$).toBeObservable(expected);
    });

    test('returns CreateTaskItemFail action on fail', () => {
      const payload = {} as any;
      const action = new fromToDoListActions.CreateTaskItem({} as any);
      const completion = new fromToDoListActions.CreateTaskItemFail(payload);

      actions = hot('-a', { a: action });
      const response = cold('-#', {}, payload);
      const expected = cold('--c', { c: completion });
      toDoListDataService.createTaskItem.mockReturnValue(response);

      expect(effects.createTaskItem$).toSatisfyOnFlush(() => {
        expect(toDoListDataService.createTaskItem).toHaveBeenCalled();
      });
      expect(effects.createTaskItem$).toBeObservable(expected);
    });
  });

  describe('updateTaskItem$', () => {
    test('returns UpdateTaskItemSuccess action on success', () => {
      const payload = {} as any;
      const action = new fromToDoListActions.UpdateTaskItem({} as any);
      const completion = new fromToDoListActions.UpdateTaskItemSuccess(payload);

      actions = hot('-a', { a: action });
      const response = cold('--b|', { b: payload });
      const expected = cold('---c', { c: completion });
      toDoListDataService.updateTaskItem.mockReturnValue(response);

      expect(effects.updateTaskItem$).toSatisfyOnFlush(() => {
        expect(toDoListDataService.updateTaskItem).toHaveBeenCalled();
      });
      expect(effects.updateTaskItem$).toBeObservable(expected);
    });

    test('returns UpdateTaskItemFail action on fail', () => {
      const payload = {} as any;
      const action = new fromToDoListActions.UpdateTaskItem({} as any);
      const completion = new fromToDoListActions.UpdateTaskItemFail(payload);

      actions = hot('-a', { a: action });
      const response = cold('-#', {}, payload);
      const expected = cold('--c', { c: completion });
      toDoListDataService.updateTaskItem.mockReturnValue(response);

      expect(effects.updateTaskItem$).toSatisfyOnFlush(() => {
        expect(toDoListDataService.updateTaskItem).toHaveBeenCalled();
      });
      expect(effects.updateTaskItem$).toBeObservable(expected);
    });
  });

  describe('removeTaskItem$', () => {
    test('returns RemoveTaskItemSuccess action on success', () => {
      const payload = {} as any;
      const action = new fromToDoListActions.RemoveTaskItem({} as any);
      const completion = new fromToDoListActions.RemoveTaskItemSuccess(payload);

      actions = hot('-a', { a: action });
      const response = cold('--b|', { b: payload });
      const expected = cold('---c', { c: completion });
      toDoListDataService.removeTaskItem.mockReturnValue(response);

      expect(effects.removeTaskItem$).toSatisfyOnFlush(() => {
        expect(toDoListDataService.removeTaskItem).toHaveBeenCalled();
      });
      expect(effects.removeTaskItem$).toBeObservable(expected);
    });

    test('returns RemoveTaskItemFail action on fail', () => {
      const payload = {} as any;
      const action = new fromToDoListActions.RemoveTaskItem({} as any);
      const completion = new fromToDoListActions.RemoveTaskItemFail(payload);

      actions = hot('-a', { a: action });
      const response = cold('-#', {}, payload);
      const expected = cold('--c', { c: completion });
      toDoListDataService.removeTaskItem.mockReturnValue(response);

      expect(effects.removeTaskItem$).toSatisfyOnFlush(() => {
        expect(toDoListDataService.removeTaskItem).toHaveBeenCalled();
      });
      expect(effects.removeTaskItem$).toBeObservable(expected);
    });
  });
});
