import { fromToDoListActions } from './to-do-list.actions';
import {
  ToDoListState,
  initialState,
  toDoListReducer,
} from './to-do-list.reducer';
import { statesEqual } from '@valueadd/testing';

describe('ToDoList Reducer', () => {
  let state: ToDoListState;

  beforeEach(() => {
    state = { ...initialState };
  });

  describe('unknown action', () => {
    test('returns the initial state', () => {
      const action = {} as any;
      const result = toDoListReducer(state, action);

      expect(result).toBe(state);
    });
  });

  describe('GetTaskItem', () => {
    test('sets taskItem, taskItemLoading, taskItemLoadError and does not modify other state properties', () => {
      const payload = {} as any;
      const action = new fromToDoListActions.GetTaskItem(payload);
      const result = toDoListReducer(state, action);

      expect(result.taskItem).toEqual(null);
      expect(result.taskItemLoading).toEqual(true);
      expect(result.taskItemLoadError).toEqual(null);
      expect(
        statesEqual(result, state, [
          'taskItem',
          'taskItemLoading',
          'taskItemLoadError',
        ])
      ).toBeTruthy();
    });
  });

  describe('GetTaskItemFail', () => {
    test('sets taskItem, taskItemLoading, taskItemLoadError and does not modify other state properties', () => {
      const payload = {} as any;
      const action = new fromToDoListActions.GetTaskItemFail(payload);
      const result = toDoListReducer(state, action);

      expect(result.taskItem).toEqual(null);
      expect(result.taskItemLoading).toEqual(false);
      expect(result.taskItemLoadError).toEqual(payload);
      expect(
        statesEqual(result, state, [
          'taskItem',
          'taskItemLoading',
          'taskItemLoadError',
        ])
      ).toBeTruthy();
    });
  });

  describe('GetTaskItemSuccess', () => {
    test('sets taskItem, taskItemLoading, taskItemLoadError and does not modify other state properties', () => {
      const payload = {} as any;
      const action = new fromToDoListActions.GetTaskItemSuccess(payload);
      const result = toDoListReducer(state, action);

      expect(result.taskItem).toEqual(payload);
      expect(result.taskItemLoading).toEqual(false);
      expect(result.taskItemLoadError).toEqual(null);
      expect(
        statesEqual(result, state, [
          'taskItem',
          'taskItemLoading',
          'taskItemLoadError',
        ])
      ).toBeTruthy();
    });
  });

  describe('GetTaskItemCollection', () => {
    test('sets taskItemCollection, taskItemCollectionLoading, taskItemCollectionLoadError and does not modify other state properties', () => {
      const payload = {} as any;
      const action = new fromToDoListActions.GetTaskItemCollection(payload);
      const result = toDoListReducer(state, action);

      expect(result.taskItemCollection).toEqual([]);
      expect(result.taskItemCollectionLoading).toEqual(true);
      expect(result.taskItemCollectionLoadError).toEqual(null);
      expect(
        statesEqual(result, state, [
          'taskItemCollection',
          'taskItemCollectionLoading',
          'taskItemCollectionLoadError',
        ])
      ).toBeTruthy();
    });
  });

  describe('GetTaskItemCollectionFail', () => {
    test('sets taskItemCollection, taskItemCollectionLoading, taskItemCollectionLoadError and does not modify other state properties', () => {
      const payload = {} as any;
      const action = new fromToDoListActions.GetTaskItemCollectionFail(payload);
      const result = toDoListReducer(state, action);

      expect(result.taskItemCollection).toEqual([]);
      expect(result.taskItemCollectionLoading).toEqual(false);
      expect(result.taskItemCollectionLoadError).toEqual(payload);
      expect(
        statesEqual(result, state, [
          'taskItemCollection',
          'taskItemCollectionLoading',
          'taskItemCollectionLoadError',
        ])
      ).toBeTruthy();
    });
  });

  describe('GetTaskItemCollectionSuccess', () => {
    test('sets taskItemCollection, taskItemCollectionLoading, taskItemCollectionLoadError and does not modify other state properties', () => {
      const payload = {} as any;
      const action = new fromToDoListActions.GetTaskItemCollectionSuccess(
        payload
      );
      const result = toDoListReducer(state, action);

      expect(result.taskItemCollection).toEqual(payload);
      expect(result.taskItemCollectionLoading).toEqual(false);
      expect(result.taskItemCollectionLoadError).toEqual(null);
      expect(
        statesEqual(result, state, [
          'taskItemCollection',
          'taskItemCollectionLoading',
          'taskItemCollectionLoadError',
        ])
      ).toBeTruthy();
    });
  });

  describe('CreateTaskItem', () => {
    test('sets taskItemCreating, taskItemCreateError and does not modify other state properties', () => {
      const payload = {} as any;
      const action = new fromToDoListActions.CreateTaskItem(payload);
      const result = toDoListReducer(state, action);

      expect(result.taskItemCreating).toEqual(true);
      expect(result.taskItemCreateError).toEqual(null);
      expect(
        statesEqual(result, state, ['taskItemCreating', 'taskItemCreateError'])
      ).toBeTruthy();
    });
  });

  describe('CreateTaskItemFail', () => {
    test('sets taskItemCreating, taskItemCreateError and does not modify other state properties', () => {
      const payload = {} as any;
      const action = new fromToDoListActions.CreateTaskItemFail(payload);
      const result = toDoListReducer(state, action);

      expect(result.taskItemCreating).toEqual(false);
      expect(result.taskItemCreateError).toEqual(payload);
      expect(
        statesEqual(result, state, ['taskItemCreating', 'taskItemCreateError'])
      ).toBeTruthy();
    });
  });

  describe('CreateTaskItemSuccess', () => {
    test('sets taskItemCollection, taskItemCreating, taskItemCreateError and does not modify other state properties', () => {
      const payload = {} as any;
      const action = new fromToDoListActions.CreateTaskItemSuccess(payload);
      const result = toDoListReducer(state, action);

      expect(result.taskItemCollection.length).toEqual(1);
      expect(result.taskItemCreating).toEqual(false);
      expect(result.taskItemCreateError).toEqual(null);
      expect(
        statesEqual(result, state, [
          'taskItemCollection',
          'taskItemCreating',
          'taskItemCreateError',
        ])
      ).toBeTruthy();
    });
  });

  describe('UpdateTaskItem', () => {
    test('sets taskItemUpdating, taskItemUpdateError and does not modify other state properties', () => {
      const payload = {} as any;
      const action = new fromToDoListActions.UpdateTaskItem(payload);
      const result = toDoListReducer(state, action);

      expect(result.taskItemUpdating).toEqual(true);
      expect(result.taskItemUpdateError).toEqual(null);
      expect(
        statesEqual(result, state, ['taskItemUpdating', 'taskItemUpdateError'])
      ).toBeTruthy();
    });
  });

  describe('UpdateTaskItemFail', () => {
    test('sets taskItemUpdating, taskItemUpdateError and does not modify other state properties', () => {
      const payload = {} as any;
      const action = new fromToDoListActions.UpdateTaskItemFail(payload);
      const result = toDoListReducer(state, action);

      expect(result.taskItemUpdating).toEqual(false);
      expect(result.taskItemUpdateError).toEqual(payload);
      expect(
        statesEqual(result, state, ['taskItemUpdating', 'taskItemUpdateError'])
      ).toBeTruthy();
    });
  });

  describe('UpdateTaskItemSuccess', () => {
    test('sets taskItemCollection, taskItemUpdating, taskItemUpdateError and does not modify other state properties', () => {
      state = {
        ...initialState,
        taskItemCollection: [{ id: '1', name: 'test' } as any],
      };
      const payload = { id: '1', name: 'test2' } as any;
      const action = new fromToDoListActions.UpdateTaskItemSuccess(payload);
      const result = toDoListReducer(state, action);

      expect((result as any).taskItemCollection[0].name).toEqual('test2');
      expect((result as any).taskItemUpdating).toEqual(false);
      expect((result as any).taskItemUpdateError).toEqual(null);
      expect(
        statesEqual(result, state, [
          'taskItemCollection',
          'taskItemUpdating',
          'taskItemUpdateError',
        ])
      ).toBeTruthy();
    });
  });

  describe('RemoveTaskItem', () => {
    test('sets taskItemRemoving, taskItemRemoveError and does not modify other state properties', () => {
      const payload = {} as any;
      const action = new fromToDoListActions.RemoveTaskItem(payload);
      const result = toDoListReducer(state, action);

      expect(result.taskItemRemoving).toEqual(true);
      expect(result.taskItemRemoveError).toEqual(null);
      expect(
        statesEqual(result, state, ['taskItemRemoving', 'taskItemRemoveError'])
      ).toBeTruthy();
    });
  });

  describe('RemoveTaskItemFail', () => {
    test('sets taskItemRemoving, taskItemRemoveError and does not modify other state properties', () => {
      const payload = {} as any;
      const action = new fromToDoListActions.RemoveTaskItemFail(payload);
      const result = toDoListReducer(state, action);

      expect(result.taskItemRemoving).toEqual(false);
      expect(result.taskItemRemoveError).toEqual(payload);
      expect(
        statesEqual(result, state, ['taskItemRemoving', 'taskItemRemoveError'])
      ).toBeTruthy();
    });
  });

  describe('RemoveTaskItemSuccess', () => {
    test('sets taskItemCollection, taskItemRemoving, taskItemRemoveError and does not modify other state properties', () => {
      state = {
        ...initialState,
        taskItemCollection: [{ id: '1', name: 'test' } as any],
      };
      const payload = { id: '1', name: 'test2' } as any;
      const action = new fromToDoListActions.RemoveTaskItemSuccess(payload);
      const result = toDoListReducer(state, action);

      expect(result.taskItemCollection.length).toEqual(0);
      expect(result.taskItemRemoving).toEqual(false);
      expect(result.taskItemRemoveError).toEqual(null);
      expect(
        statesEqual(result, state, [
          'taskItemCollection',
          'taskItemRemoving',
          'taskItemRemoveError',
        ])
      ).toBeTruthy();
    });
  });
});
