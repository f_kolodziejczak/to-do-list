import {
  initialState,
  TODOLIST_FEATURE_KEY,
  ToDoListState,
} from './to-do-list.reducer';
import { toDoListQuery } from './to-do-list.selectors';

describe('ToDoList Selectors', () => {
  let storeState: { [TODOLIST_FEATURE_KEY]: ToDoListState };

  beforeEach(() => {
    storeState = {
      [TODOLIST_FEATURE_KEY]: initialState,
    };
  });

  test('getTaskItem() returns taskItem value', () => {
    const result = toDoListQuery.getTaskItem(storeState);

    expect(result).toBe(storeState[TODOLIST_FEATURE_KEY].taskItem);
  });

  test('getTaskItemLoading() returns taskItemLoading value', () => {
    const result = toDoListQuery.getTaskItemLoading(storeState);

    expect(result).toBe(storeState[TODOLIST_FEATURE_KEY].taskItemLoading);
  });

  test('getTaskItemLoadError() returns taskItemLoadError value', () => {
    const result = toDoListQuery.getTaskItemLoadError(storeState);

    expect(result).toBe(storeState[TODOLIST_FEATURE_KEY].taskItemLoadError);
  });

  test('getTaskItemCollection() returns taskItemCollection value', () => {
    const result = toDoListQuery.getTaskItemCollection(storeState);

    expect(result).toBe(storeState[TODOLIST_FEATURE_KEY].taskItemCollection);
  });

  test('getTaskItemCollectionLoading() returns taskItemCollectionLoading value', () => {
    const result = toDoListQuery.getTaskItemCollectionLoading(storeState);

    expect(result).toBe(
      storeState[TODOLIST_FEATURE_KEY].taskItemCollectionLoading
    );
  });

  test('getTaskItemCollectionLoadError() returns taskItemCollectionLoadError value', () => {
    const result = toDoListQuery.getTaskItemCollectionLoadError(storeState);

    expect(result).toBe(
      storeState[TODOLIST_FEATURE_KEY].taskItemCollectionLoadError
    );
  });

  test('getTaskItemCreating() returns taskItemCreating value', () => {
    const result = toDoListQuery.getTaskItemCreating(storeState);

    expect(result).toBe(storeState[TODOLIST_FEATURE_KEY].taskItemCreating);
  });

  test('getTaskItemCreateError() returns taskItemCreateError value', () => {
    const result = toDoListQuery.getTaskItemCreateError(storeState);

    expect(result).toBe(storeState[TODOLIST_FEATURE_KEY].taskItemCreateError);
  });

  test('getTaskItemUpdating() returns taskItemUpdating value', () => {
    const result = toDoListQuery.getTaskItemUpdating(storeState);

    expect(result).toBe(storeState[TODOLIST_FEATURE_KEY].taskItemUpdating);
  });

  test('getTaskItemUpdateError() returns taskItemUpdateError value', () => {
    const result = toDoListQuery.getTaskItemUpdateError(storeState);

    expect(result).toBe(storeState[TODOLIST_FEATURE_KEY].taskItemUpdateError);
  });

  test('getTaskItemRemoving() returns taskItemRemoving value', () => {
    const result = toDoListQuery.getTaskItemRemoving(storeState);

    expect(result).toBe(storeState[TODOLIST_FEATURE_KEY].taskItemRemoving);
  });

  test('getTaskItemRemoveError() returns taskItemRemoveError value', () => {
    const result = toDoListQuery.getTaskItemRemoveError(storeState);

    expect(result).toBe(storeState[TODOLIST_FEATURE_KEY].taskItemRemoveError);
  });
});
