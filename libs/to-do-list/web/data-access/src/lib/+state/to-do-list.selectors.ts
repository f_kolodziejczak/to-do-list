import { createFeatureSelector, createSelector } from '@ngrx/store';
import { TODOLIST_FEATURE_KEY, ToDoListState } from './to-do-list.reducer';
import { Status } from '../../../../domain/src';

// Lookup the 'ToDoList' feature state managed by NgRx
const getToDoListState = createFeatureSelector<ToDoListState>(
  TODOLIST_FEATURE_KEY
);

const getTaskItem = createSelector(getToDoListState, (state) => state.taskItem);

const getTaskItemLoading = createSelector(
  getToDoListState,
  (state) => state.taskItemLoading
);

const getTaskItemLoadError = createSelector(
  getToDoListState,
  (state) => state.taskItemLoadError
);

const getTaskItemCollectionToDo = createSelector(
  getToDoListState,
  (state) => state.taskItemCollection.filter((taskItem) =>
  taskItem.status === Status.ToDo)
);

const getTaskItemCollectionDone = createSelector(
  getToDoListState,
  (state) => state.taskItemCollection.filter((taskItem) =>
    taskItem.status === Status.Done)
);

const getTaskItemCollection = createSelector(
  getToDoListState,
  (state) => state.taskItemCollection
);

const getTaskItemCollectionLoading = createSelector(
  getToDoListState,
  (state) => state.taskItemCollectionLoading
);

const getTaskItemCollectionLoadError = createSelector(
  getToDoListState,
  (state) => state.taskItemCollectionLoadError
);

const getTaskItemCreating = createSelector(
  getToDoListState,
  (state) => state.taskItemCreating
);

const getTaskItemCreateError = createSelector(
  getToDoListState,
  (state) => state.taskItemCreateError
);

const getTaskItemUpdating = createSelector(
  getToDoListState,
  (state) => state.taskItemUpdating
);

const getTaskItemUpdateError = createSelector(
  getToDoListState,
  (state) => state.taskItemUpdateError
);

const getTaskItemRemoving = createSelector(
  getToDoListState,
  (state) => state.taskItemRemoving
);

const getTaskItemRemoveError = createSelector(
  getToDoListState,
  (state) => state.taskItemRemoveError
);

export const toDoListQuery = {
  getTaskItem,
  getTaskItemLoading,
  getTaskItemLoadError,
  getTaskItemCollectionToDo,
  getTaskItemCollectionDone,
  getTaskItemCollection,
  getTaskItemCollectionLoading,
  getTaskItemCollectionLoadError,
  getTaskItemCreating,
  getTaskItemCreateError,
  getTaskItemUpdating,
  getTaskItemUpdateError,
  getTaskItemRemoving,
  getTaskItemRemoveError,
};
