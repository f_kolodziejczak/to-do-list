import { Injectable } from '@angular/core';
import { Effect } from '@ngrx/effects';
import { DataPersistence } from '@nrwl/angular';
import { fromToDoListActions } from './to-do-list.actions';
import { ToDoListPartialState } from './to-do-list.reducer';
import { ToDoListDataService } from '../services/to-do-list-data.service';
import { HttpErrorResponse } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable()
export class ToDoListEffects {
  @Effect()
  getTaskItem$ = this.dp.fetch(fromToDoListActions.Types.GetTaskItem, {
    id: () => {},
    run: (action: fromToDoListActions.GetTaskItem) => {
      return this.toDoListDataService
        .getTaskItem(action.payload)
        .pipe(map((data) => new fromToDoListActions.GetTaskItemSuccess(data)));
    },
    onError: (
      action: fromToDoListActions.GetTaskItem,
      error: HttpErrorResponse
    ) => {
      return new fromToDoListActions.GetTaskItemFail(error);
    },
  });

  @Effect()
  getTaskItemCollection$ = this.dp.fetch(
    fromToDoListActions.Types.GetTaskItemCollection,
    {
      id: () => {},
      run: (action: fromToDoListActions.GetTaskItemCollection) => {
        return this.toDoListDataService
          .getTaskItemCollection()
          .pipe(
            map(
              (data) =>
                new fromToDoListActions.GetTaskItemCollectionSuccess(data)
            )
          );
      },
      onError: (
        action: fromToDoListActions.GetTaskItemCollection,
        error: HttpErrorResponse
      ) => {
        return new fromToDoListActions.GetTaskItemCollectionFail(error);
      },
    }
  );

  @Effect()
  createTaskItem$ = this.dp.pessimisticUpdate(
    fromToDoListActions.Types.CreateTaskItem,
    {
      run: (action: fromToDoListActions.CreateTaskItem) => {
        return this.toDoListDataService
          .createTaskItem(action.payload)
          .pipe(
            map((data) => new fromToDoListActions.CreateTaskItemSuccess(data))
          );
      },
      onError: (
        action: fromToDoListActions.CreateTaskItem,
        error: HttpErrorResponse
      ) => {
        console.log(error);
        return new fromToDoListActions.CreateTaskItemFail(error);
      },
    }
  );

  @Effect()
  updateTaskItem$ = this.dp.pessimisticUpdate(
    fromToDoListActions.Types.UpdateTaskItem,
    {
      run: (action: fromToDoListActions.UpdateTaskItem) => {
        return this.toDoListDataService
          .updateTaskItem(action.payload)
          .pipe(
            map((data) => new fromToDoListActions.UpdateTaskItemSuccess(data))
          );
      },
      onError: (
        action: fromToDoListActions.UpdateTaskItem,
        error: HttpErrorResponse
      ) => {
        return new fromToDoListActions.UpdateTaskItemFail(error);
      },
    }
  );

  @Effect()
  removeTaskItem$ = this.dp.pessimisticUpdate(
    fromToDoListActions.Types.RemoveTaskItem,
    {
      run: (action: fromToDoListActions.RemoveTaskItem) => {
        return this.toDoListDataService
          .removeTaskItem(action.payload)
          .pipe(
            map(
              (data) =>
                new fromToDoListActions.RemoveTaskItemSuccess(action.payload)
            )
          );
      },
      onError: (
        action: fromToDoListActions.RemoveTaskItem,
        error: HttpErrorResponse
      ) => {
        return new fromToDoListActions.RemoveTaskItemFail(error);
      },
    }
  );

  constructor(
    private dp: DataPersistence<ToDoListPartialState>,
    private toDoListDataService: ToDoListDataService
  ) {}
}
