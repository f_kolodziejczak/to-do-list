import { NgModule } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { NxModule } from '@nrwl/angular';
import { Observable } from 'rxjs';
import { ToDoListFacade } from './to-do-list.facade';
import { fromToDoListActions } from './to-do-list.actions';

describe('ToDoListFacade', () => {
  let actions: Observable<any>;
  let facade: ToDoListFacade;
  let store: MockStore;

  describe('used in NgModule', () => {
    beforeEach(() => {
      @NgModule({
        imports: [],
        providers: [
          ToDoListFacade,
          provideMockStore(),
          provideMockActions(() => actions),
        ],
      })
      class CustomFeatureModule {}

      @NgModule({
        imports: [NxModule.forRoot(), CustomFeatureModule],
      })
      class RootModule {}

      TestBed.configureTestingModule({ imports: [RootModule] });
      facade = TestBed.inject(ToDoListFacade);
      store = TestBed.inject(MockStore);
      jest.spyOn(store, 'dispatch');
    });

    describe('#getTaskItem', () => {
      test('should dispatch fromToDoListActions.GetTaskItem action', () => {
        const payload = {} as any;
        const action = new fromToDoListActions.GetTaskItem(payload);

        facade.getTaskItem(payload);
        expect(store.dispatch).toHaveBeenCalledWith(action);
      });
    });

    describe('#getTaskItemCollection', () => {
      test('should dispatch fromToDoListActions.GetTaskItemCollection action', () => {
        const payload = {} as any;
        const action = new fromToDoListActions.GetTaskItemCollection(payload);

        facade.getTaskItemCollection(payload);
        expect(store.dispatch).toHaveBeenCalledWith(action);
      });
    });

    describe('#createTaskItem', () => {
      test('should dispatch fromToDoListActions.CreateTaskItem action', () => {
        const payload = {} as any;
        const action = new fromToDoListActions.CreateTaskItem(payload);

        facade.createTaskItem(payload);
        expect(store.dispatch).toHaveBeenCalledWith(action);
      });
    });

    describe('#updateTaskItem', () => {
      test('should dispatch fromToDoListActions.UpdateTaskItem action', () => {
        const payload = {} as any;
        const action = new fromToDoListActions.UpdateTaskItem(payload);

        facade.updateTaskItem(payload);
        expect(store.dispatch).toHaveBeenCalledWith(action);
      });
    });

    describe('#removeTaskItem', () => {
      test('should dispatch fromToDoListActions.RemoveTaskItem action', () => {
        const payload = {} as any;
        const action = new fromToDoListActions.RemoveTaskItem(payload);

        facade.removeTaskItem(payload);
        expect(store.dispatch).toHaveBeenCalledWith(action);
      });
    });
  });
});
