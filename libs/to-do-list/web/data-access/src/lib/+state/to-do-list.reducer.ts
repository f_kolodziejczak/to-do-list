import { fromToDoListActions } from './to-do-list.actions';
import { HttpErrorResponse } from '@angular/common/http';
import { TaskItem } from '../../../../domain/src';

export const TODOLIST_FEATURE_KEY = 'toDoList';

export interface ToDoListState {
  taskItem: TaskItem | null;
  taskItemLoading: boolean;
  taskItemLoadError: HttpErrorResponse | null;
  taskItemCollection: TaskItem[];
  taskItemCollectionLoading: boolean;
  taskItemCollectionLoadError: HttpErrorResponse | null;
  taskItemCreating: boolean;
  taskItemCreateError: HttpErrorResponse | null;
  taskItemUpdating: boolean;
  taskItemUpdateError: HttpErrorResponse | null;
  taskItemRemoving: boolean;
  taskItemRemoveError: HttpErrorResponse | null;
}

export interface ToDoListPartialState {
  readonly [TODOLIST_FEATURE_KEY]: ToDoListState;
}

export const initialState: ToDoListState = {
  taskItem: null,
  taskItemLoading: false,
  taskItemLoadError: null,
  taskItemCollection: [],
  taskItemCollectionLoading: false,
  taskItemCollectionLoadError: null,
  taskItemCreating: false,
  taskItemCreateError: null,
  taskItemUpdating: false,
  taskItemUpdateError: null,
  taskItemRemoving: false,
  taskItemRemoveError: null,
};

export function toDoListReducer(
  state: ToDoListState = initialState,
  action: fromToDoListActions.CollectiveType
): ToDoListState {
  switch (action.type) {
    case fromToDoListActions.Types.GetTaskItem: {
      state = {
        ...state,
        taskItem: null,
        taskItemLoading: true,
        taskItemLoadError: null,
      };
      break;
    }

    case fromToDoListActions.Types.GetTaskItemFail: {
      state = {
        ...state,
        taskItem: null,
        taskItemLoading: false,
        taskItemLoadError: action.payload,
      };
      break;
    }

    case fromToDoListActions.Types.GetTaskItemSuccess: {
      state = {
        ...state,
        taskItem: action.payload,
        taskItemLoading: false,
        taskItemLoadError: null,
      };
      break;
    }

    case fromToDoListActions.Types.GetTaskItemCollection: {
      state = {
        ...state,
        taskItemCollection: [],
        taskItemCollectionLoading: true,
        taskItemCollectionLoadError: null,
      };
      break;
    }

    case fromToDoListActions.Types.GetTaskItemCollectionFail: {
      state = {
        ...state,
        taskItemCollection: [],
        taskItemCollectionLoading: false,
        taskItemCollectionLoadError: action.payload,
      };
      break;
    }

    case fromToDoListActions.Types.GetTaskItemCollectionSuccess: {
      state = {
        ...state,
        taskItemCollection: action.payload,
        taskItemCollectionLoading: false,
        taskItemCollectionLoadError: null,
      };
      break;
    }

    case fromToDoListActions.Types.CreateTaskItem: {
      state = {
        ...state,
        taskItemCreating: true,
        taskItemCreateError: null,
      };
      break;
    }

    case fromToDoListActions.Types.CreateTaskItemFail: {
      state = {
        ...state,
        taskItem: state.taskItem,
        taskItemCreating: false,
        taskItemCreateError: action.payload,
      };
      break;
    }

    case fromToDoListActions.Types.CreateTaskItemSuccess: {
      state = {
        ...state,
        taskItemCollection: state.taskItemCollection.concat(action.payload),
        taskItemCreating: false,
        taskItemCreateError: null,
      };
      break;
    }

    case fromToDoListActions.Types.UpdateTaskItem: {
      state = {
        ...state,
        taskItemUpdating: true,
        taskItemUpdateError: null,
      };
      break;
    }

    case fromToDoListActions.Types.UpdateTaskItemFail: {
      state = {
        ...state,
        taskItemUpdating: false,
        taskItemUpdateError: action.payload,
      };
      break;
    }

    case fromToDoListActions.Types.UpdateTaskItemSuccess: {
      const taskItemCollectionToUpdate = [...state.taskItemCollection];
      const taskToUpdate = taskItemCollectionToUpdate.find((taskItem) => taskItem.id === action.payload.id);
      taskItemCollectionToUpdate[taskItemCollectionToUpdate.indexOf(taskToUpdate)] = action.payload;
        state = {
        ...state,
        taskItemCollection: taskItemCollectionToUpdate,
        taskItemUpdating: false,
        taskItemUpdateError: null,
      };
      break;
    }

    case fromToDoListActions.Types.RemoveTaskItem: {
      state = {
        ...state,
        taskItemRemoving: true,
        taskItemRemoveError: null,
      };
      break;
    }

    case fromToDoListActions.Types.RemoveTaskItemFail: {
      state = {
        ...state,
        taskItemRemoving: false,
        taskItemRemoveError: action.payload,
      };
      break;
    }

    case fromToDoListActions.Types.RemoveTaskItemSuccess: {
      state = {
        ...state,
        taskItemCollection: state.taskItemCollection.filter(
          (e) => e.id !== action.payload.id
        ),
        taskItemRemoving: false,
        taskItemRemoveError: null,
      };
      break;
    }
  }

  return state;
}
