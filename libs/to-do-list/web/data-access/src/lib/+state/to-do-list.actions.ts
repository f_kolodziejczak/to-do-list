import { Action } from '@ngrx/store';
import { HttpErrorResponse } from '@angular/common/http';
import { GetTaskItemRequestPayload } from '../resources/request-payloads/get-task-item.request-payload';
import { CreateTaskItemRequestPayload } from '../resources/request-payloads/create-task-item.request-payload';
import { UpdateTaskItemRequestPayload } from '../resources/request-payloads/update-task-item.request-payload';
import { RemoveTaskItemRequestPayload } from '../resources/request-payloads/remove-task-item.request-payload';
import { TaskItem } from '../../../../domain/src';

// eslint-disable-next-line @typescript-eslint/no-namespace
export namespace fromToDoListActions {
  export enum Types {
    GetTaskItem = '[todo] Get Task Item',
    GetTaskItemFail = '[todo] Get Task Item Fail',
    GetTaskItemSuccess = '[todo] Get Task Item Success',
    GetTaskItemCollection = '[todo] Get Task Item Collection',
    GetTaskItemCollectionFail = '[todo] Get Task Item Collection Fail',
    GetTaskItemCollectionSuccess = '[todo] Get Task Item Collection Success',
    CreateTaskItem = '[todo] Create Task Item',
    CreateTaskItemFail = '[todo] Create Task Item Fail',
    CreateTaskItemSuccess = '[todo] Create Task Item Success',
    UpdateTaskItem = '[todo] Update Task Item',
    UpdateTaskItemFail = '[todo] Update Task Item Fail',
    UpdateTaskItemSuccess = '[todo] Update Task Item Success',
    RemoveTaskItem = '[todo] Remove Task Item',
    RemoveTaskItemFail = '[todo] Remove Task Item Fail',
    RemoveTaskItemSuccess = '[todo] Remove Task Item Success',
  }

  export class GetTaskItem implements Action {
    readonly type = Types.GetTaskItem;

    constructor(public payload: GetTaskItemRequestPayload) {
    }
  }

  export class GetTaskItemFail implements Action {
    readonly type = Types.GetTaskItemFail;

    constructor(public payload: HttpErrorResponse) {
    }
  }

  export class GetTaskItemSuccess implements Action {
    readonly type = Types.GetTaskItemSuccess;

    constructor(public payload: TaskItem) {
    }
  }

  export class GetTaskItemCollection implements Action {
    readonly type = Types.GetTaskItemCollection;
  }

  export class GetTaskItemCollectionFail implements Action {
    readonly type = Types.GetTaskItemCollectionFail;

    constructor(public payload: HttpErrorResponse) {
    }
  }

  export class GetTaskItemCollectionSuccess implements Action {
    readonly type = Types.GetTaskItemCollectionSuccess;

    constructor(public payload: TaskItem[]) {
    }
  }

  export class CreateTaskItem implements Action {
    readonly type = Types.CreateTaskItem;
    constructor(public payload: CreateTaskItemRequestPayload) {
    }
  }

  export class CreateTaskItemFail implements Action {
    readonly type = Types.CreateTaskItemFail;

    constructor(public payload: HttpErrorResponse) {
    }
  }

  export class CreateTaskItemSuccess implements Action {
    readonly type = Types.CreateTaskItemSuccess;

    constructor(public payload: TaskItem) {
    }
  }

  export class UpdateTaskItem implements Action {
    readonly type = Types.UpdateTaskItem;

    constructor(public payload: UpdateTaskItemRequestPayload) {
    }
  }

  export class UpdateTaskItemFail implements Action {
    readonly type = Types.UpdateTaskItemFail;

    constructor(public payload: HttpErrorResponse) {
    }
  }

  export class UpdateTaskItemSuccess implements Action {
    readonly type = Types.UpdateTaskItemSuccess;

    constructor(public payload: TaskItem) {
    }
  }

  export class RemoveTaskItem implements Action {
    readonly type = Types.RemoveTaskItem;

    constructor(public payload: RemoveTaskItemRequestPayload) {
    }
  }

  export class RemoveTaskItemFail implements Action {
    readonly type = Types.RemoveTaskItemFail;

    constructor(public payload: HttpErrorResponse) {
    }
  }

  export class RemoveTaskItemSuccess implements Action {
    readonly type = Types.RemoveTaskItemSuccess;

    constructor(public payload: RemoveTaskItemRequestPayload) {
    }
  }

  export type CollectiveType =
    | GetTaskItem
    | GetTaskItemFail
    | GetTaskItemSuccess
    | GetTaskItemCollection
    | GetTaskItemCollectionFail
    | GetTaskItemCollectionSuccess
    | CreateTaskItem
    | CreateTaskItemFail
    | CreateTaskItemSuccess
    | UpdateTaskItem
    | UpdateTaskItemFail
    | UpdateTaskItemSuccess
    | RemoveTaskItem
    | RemoveTaskItemFail
    | RemoveTaskItemSuccess;
}
