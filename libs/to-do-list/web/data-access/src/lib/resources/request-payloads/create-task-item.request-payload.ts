import { TaskItem } from '../../../../../domain/src';

export interface CreateTaskItemRequestPayload {
  data: TaskItem;
}
