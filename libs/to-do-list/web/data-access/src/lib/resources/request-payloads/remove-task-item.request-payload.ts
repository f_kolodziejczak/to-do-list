export interface RemoveTaskItemRequestPayload {
  id: string | number;
}
