import { TaskItem } from '../../../../../domain/src';

export interface UpdateTaskItemRequestPayload {
  data: TaskItem;
}
