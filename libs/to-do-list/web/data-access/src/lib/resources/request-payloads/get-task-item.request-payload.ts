export interface GetTaskItemRequestPayload {
  id: string | number;
}
