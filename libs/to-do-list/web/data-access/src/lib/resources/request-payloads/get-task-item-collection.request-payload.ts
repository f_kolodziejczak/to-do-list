import { TaskItem } from '../../../../../domain/src';

export interface GetTaskItemCollectionRequestPayload {
  data: Array<TaskItem>;
}
