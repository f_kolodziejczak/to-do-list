
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import {
  initialState as toDoListInitialState, TODOLIST_FEATURE_KEY,
  toDoListReducer
} from './+state/to-do-list.reducer';
import { ToDoListEffects } from './+state/to-do-list.effects';
import { ToDoListFacade } from './+state/to-do-list.facade';
import { ToDoListDataService } from './services/to-do-list-data.service';

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature(TODOLIST_FEATURE_KEY, toDoListReducer),
    EffectsModule.forRoot([ToDoListEffects]),
  ],
  providers: [ToDoListFacade, ToDoListDataService],
})
export class ToDoListWebDataAccessModule {}
