import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FeatureModule } from '../../../../feature/src/lib/feature/feature.module';



@NgModule({
  imports: [
    CommonModule,
    FeatureModule,
  ]
})
export class ToDoListWebShellModule { }
