# to-do-list-web-shell

This library was generated with [Nx](https://nx.dev).

## Running unit tests

Run `nx test to-do-list-web-shell` to execute the unit tests via [Jest](https://jestjs.io).
