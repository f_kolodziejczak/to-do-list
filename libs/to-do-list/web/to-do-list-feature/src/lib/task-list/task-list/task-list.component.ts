import {
  ChangeDetectionStrategy,
  Component, EventEmitter,
  Input, Output,
} from '@angular/core';
import { TaskItem } from '../../../../../domain/src/lib/interfaces/task-item.interface';
import { Status } from '../../../../../domain/src/lib/enums/status.enum';

@Component({
  selector: 'valueadd-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TaskListComponent {

  @Input() title = '';
  @Input() readonly tasks: TaskItem[] = [];
  @Output() taskStatusChanged = new EventEmitter<TaskItem>();
  @Output() taskDeleted = new EventEmitter<string>();
  @Output() taskEdited = new EventEmitter<TaskItem>();
  status = [Status.ToDo, Status.Done];

  getColor(priority): string {
    switch(priority) {
      case 'High':
        return 'red';
      case 'Medium':
        return 'orange';
      case 'Low':
        return 'green';
    }
  }
}
