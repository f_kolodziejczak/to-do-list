import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TasksComponent } from './tasks/tasks.component';
import { TaskItemComponent } from './tasks/task-item/task-item.component';
import { SharedModule } from './shared/shared.module';
import { RouterModule } from '@angular/router';
import { TaskListModule } from '../../../../to-do-list-feature/src/lib/task-list/task-list.module';
import { ToDoListFacade } from '../../../../data-access/src/lib/+state/to-do-list.facade';
import { ToDoListWebSharedUiConfirmationDialogModule } from '../../../../shared/ui-confirmation-dialog/src';
import { ToDoListWebSharedUiGlobalHeaderModule } from '../../../../shared/ui-global-header/src';

@NgModule({
  declarations: [
    TasksComponent,
    TaskItemComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild([
      { path: '', component: TasksComponent }
    ]),
    TaskListModule,
    ToDoListWebSharedUiConfirmationDialogModule,
    ToDoListWebSharedUiGlobalHeaderModule,
  ],
  providers: [
    ToDoListFacade,
  ]
})
export class FeatureModule { }
