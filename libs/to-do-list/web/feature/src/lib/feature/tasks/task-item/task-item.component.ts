import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  OnInit,
} from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormBuilder, Validators } from '@angular/forms';
import { Priority, TaskItem } from '../../../../../../domain/src';

export interface TaskForm {
  text: string;
  priority: Priority;
}

@Component({
  selector: 'valueadd-task-item',
  templateUrl: './task-item.component.html',
  styleUrls: ['./task-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TaskItemComponent implements OnInit {
  taskItemForm = this.formBuilder.group({
    text: ['', Validators.required],
    priority: [''],
  });
  priorities = Object.values(Priority);

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: TaskItem,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {
    if (this.data) {
      this.setFormValue();
    }
  }

  setFormValue(): void {
    this.taskItemForm.patchValue({
      text: this.data.text,
      priority: this.data.priority,
    });
  }
}
