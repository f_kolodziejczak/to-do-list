import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { TaskForm, TaskItemComponent } from './task-item/task-item.component';
import { filter } from 'rxjs/operators';
import { ToDoListFacade } from '../../../../../data-access/src/lib/+state/to-do-list.facade';
import { v4 as uuidv4 } from 'uuid';
import { noop } from 'rxjs';
import { Status } from '../../../../../domain/src';
import { TaskItem } from '../../../../../domain/src';
import { ConfirmDialogComponent } from '@valueadd/to-do-list/web/shared/ui-confirmation-dialog';

@Component({
  selector: 'valueadd-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.scss'],
})
export class TasksComponent {
  tasksDoTo$ = this.toDoListFacade.taskItemCollectionToDo$;
  tasksDone$ = this.toDoListFacade.taskItemCollectionDone$;

  constructor(
    private dialog: MatDialog,
    private toDoListFacade: ToDoListFacade
  ) {
    this.toDoListFacade.getTaskItemCollection();
  }

  addTask(taskForm: TaskForm): void {
    const data = { ...taskForm, id: uuidv4(), status: Status.ToDo };
    this.toDoListFacade.createTaskItem({ data: data });
  }

  updateTask(taskForm: TaskForm, task: TaskItem): void {
    const taskToUpdate = {
      ...task,
      text: taskForm.text,
      priority: taskForm.priority,
    };
    this.toDoListFacade.updateTaskItem({ data: taskToUpdate });
  }

  openDialog(task?: TaskItem): void {
    this.dialog
      .open(TaskItemComponent, {
        minWidth: '600px',
        minHeight: '400px',
        data: task ? task : noop(),
      })
      .afterClosed()
      .pipe(filter((confirmed) => !!confirmed))
      .subscribe((result) => {
        if (task) this.updateTask(result, task);
        else this.addTask(result);
      });
  }

  changeTaskStatus(task: TaskItem): void {
    const taskToUpdate = { ...task };
    switch (taskToUpdate.status) {
      case Status.ToDo:
        taskToUpdate.status = Status.Done;
        break;
      case Status.Done:
        taskToUpdate.status = Status.ToDo;
        break;
    }
    this.toDoListFacade.updateTaskItem({ data: taskToUpdate });
  }

  deleteTask(taskId: string): void {
    this.dialog
      .open(ConfirmDialogComponent, {
        minWidth: '400px',
        data: {
          title: 'Delete Task',
          text: 'Are you sure you want to delete this task?',
          confirmButtonText: 'Confirm',
          cancelButtonText: 'Cancel',
        },
      })
      .afterClosed()
      .pipe(filter((confirmed) => !!confirmed))
      .subscribe(() => this.toDoListFacade.removeTaskItem({ id: taskId }));
  }
}
