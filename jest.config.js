module.exports = {
  projects: [
    '<rootDir>/apps/feature-list',
    '<rootDir>/libs/feature-list/web/feature-list-feature',
    '<rootDir>/libs/feature-list/web/shell',
    '<rootDir>/libs/feature',
    '<rootDir>/libs/to-do-list/web/data-access',
    '<rootDir>/libs/to-do-list/web/shared/ui-confirmation-dialog',
    '<rootDir>/libs/to-do-list/web/shared/ui-global-header',
  ],
};
